#!/bin/bash


for myfile in $(ls -1 *.html *.htm);do 
test -e $myfile.backup || cp $myfile $myfile.backup

for targetfile in $(ls -1 *.png *.PNG *.gif *.GIF *.jpg *.JPG *.JPEG *.JPEG);do

newfile=${myfile/*.html/}".inline.html"
cp $myfile $myfile.new
cat $myfile.new|while read line;do 
## print lines without our target as img src or url()
  LINEFOUND=no
  ## if the url() is without quotes , add them so the regex finds it, same for src
  line=$(echo "$line" |sed 's/url('$targetfile')/url("'$targetfile'")/g;s/src='$targetfile'/src="'$targetfile'"/g')
  [[ ! "$line" =~ .*src=.$targetfile..* ]]  && [[ ! "$line" =~ .*url\(.$targetfile.\).* ]]  && echo $line;
  [[   "$line" =~ .*src=.$targetfile..* ]]  && LINEFOUND=yes
  [[   "$line" =~ .*url\(.$targetfile.\).* ]] && LINEFOUND=yes 
  [[ "${LINEFOUND}" = "yes" ]] && { 
      echo REPLACELINE-img $line >&2 ;
      ## take the scissors, cut before and after 
      prefix=${line/$targetfile*/} ;
      suffix=${line/*$targetfile/} ;
      filetype=${targetfile/*./};
      mimetype=$(echo $filetype | tr '[:upper:]' '[:lower:]' |sed 's/jpg/jpeg/g;s/svg/svg+xml/g' )
      echo "pre $prefix | suff $suffix" >&2 ;    
      ##data:image/svg+xml
      ##data:image/png
      ##data:image/jpeg
      ##data:image/gif
      echo filetype $filetype  mimetype $mimetype >&2;
      echo $prefix'data:image/'$mimetype';base64,'$(base64 -w0 $targetfile)$suffix ; }

      done > $myfile
cat $myfile > $myfile.new 
     done
cat $myfile.new > $myfile
rm $myfile.new
done
